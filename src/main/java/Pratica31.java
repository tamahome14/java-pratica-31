/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jorgejunior
 */
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author Jorge Otta Júnior
 */
public class Pratica31 {

    private static Date inicio;
    private static String meuNome = "Jorge Otta Junior";
    /*                               0123456789012345678 */
    private static GregorianCalendar dataNascimento;
    private static Date fim;

    public static void main(String[] args) {
        inicio = new Date();
        System.out.println(meuNome.toUpperCase());
        System.out.println(meuNome.toUpperCase().charAt(6) + meuNome.toLowerCase().substring(7,10) + ", " + meuNome.toUpperCase().charAt(0) + ". " + meuNome.toUpperCase().charAt(11) + ". ");
        dataNascimento = new GregorianCalendar(1975, Calendar.SEPTEMBER, 29);        
        System.out.println((float)(inicio.getTime() - dataNascimento.getTimeInMillis())/86400000);
        fim = new Date();
        System.out.println((float)(fim.getTime() - inicio.getTime()));
    }
}

